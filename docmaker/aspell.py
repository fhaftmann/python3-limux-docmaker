
"""Wrapper around GNU aspell."""

# Author: 2015 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['spellimprove', 'spellcheck']


from collections.abc import Sequence

from . import util
import subprocess


def aspell_args(lang: str, dict: str) -> Sequence[str]:

    return ['aspell', f'--encoding={util.utf8}', f'--lang={lang}',
      f'--home-dir={dict}/', '--add-filter=url',
      '--add-filter=context', '--clear-context-delimiters', '--context-visible-first',
      '--add-context-delimiters=@{ },@#{ }']


def spellimprove(loc: str, language: str, dictionary: str) -> None:

    subprocess.run([*aspell_args(language, dictionary), '--save-repl', 'check', loc],
      check = True)


def spellcheck(loc: str, language: str, dictionary: str) -> bool:

    with open(loc) as reader:
        content = reader.read()
    output = util.pipe_through(content, [*aspell_args(language, dictionary), 'list'])
    return not bool(output)


"""Command line interface."""

# Author: 2015 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['cmd']


from typing import NoReturn, Optional
from collections.abc import Callable
import argparse
import argcomplete
from email import utils
import os
import locale
import sys

import docmaker
from . import util
from . import aspell
from .pandoc import pandoc
from . import manpage


now = utils.formatdate()


def global_runtime_setup() -> None:

    # ad-hoc fix of bad alias C.UTF-8 -> en_US.UTF-8, cf. https://bugs.python.org/issue30755
    del locale.locale_alias['c.utf8']

    # runtime-patching of import path
    cwd = os.getcwd()
    if any(loc not in sys.path for loc in ('', os.curdir, cwd)):
        sys.path.insert(0, cwd)


def fail(txt: str) -> NoReturn:

    print(txt, file = sys.stderr)
    raise SystemExit(1)


def spellimprove(args: argparse.Namespace) -> None:

    for loc in args.src:
        aspell.spellimprove(loc, args.language, args.dictionary)


def spellcheck(args: argparse.Namespace) -> None:

    for loc in args.src:
        success = aspell.spellcheck(loc, args.language, args.dictionary)
        if not success:
            fail('Spellcheck not successful for {0}'.format(loc))


def html(args: argparse.Namespace) -> None:

    txt1 = pandoc('% {0}\n'.format(args.title), args.src, 'html', date = now)
    txt2 = util.pipe_through(txt1, ['xmllint', '--nonet', '--html', '--format', '-'])
    print(txt2)


def man(args: argparse.Namespace) -> None:

    if not util.is_ident(args.manpage_name):
        fail('Illegal man page name: {0}'.format(args.manpage_name))
    txt = manpage.of_markdown(args.manpage_name, now, args.section, args.category,
      args.command_name, args.description, args.src)
    if args.output_dir:
        manpage.write_man(args.output_dir, args.manpage_name, args.section, txt)
    else:
        print(txt)


def help2man(args: argparse.Namespace) -> None:

    if not util.is_ident(args.manpage_name):
        fail('Illegal man page name: {0}'.format(args.manpage_name))
    txt = manpage.of_help(args.manpage_name, args.version, args.section, [args.cmd] + args.arg)
    if args.output_dir:
        manpage.write_man(args.output_dir, args.manpage_name, args.section, txt)
    else:
        print(txt)


def cmd() -> int:

    parser = argparse.ArgumentParser(description = docmaker.__doc__.strip())
    subparsers = parser.add_subparsers(required = True)

    def add_parser(subcommand: Callable[[argparse.Namespace], None],
      help: str, epilog: Optional[str] = None) -> argparse.ArgumentParser:

        sub_parser = subparsers.add_parser(subcommand.__name__.replace('_', '-'),
          help = help, epilog = epilog)
        sub_parser.set_defaults(subcommand = subcommand)
        return sub_parser

    sub_parser = add_parser(spellimprove, 'Spellchecking and improving documentation files.')
    sub_parser.add_argument('language')
    sub_parser.add_argument('dictionary')
    sub_parser.add_argument('src', nargs = '+')

    sub_parser = add_parser(spellcheck, 'Spellchecking documentation files.')
    sub_parser.add_argument('language')
    sub_parser.add_argument('dictionary')
    sub_parser.add_argument('src', nargs = '+')

    sub_parser = add_parser(html, 'Producing HTML documentation.')
    sub_parser.add_argument('title')
    sub_parser.add_argument('src', nargs = '+')

    sub_parser = add_parser(man, 'Producing man page.')
    sub_parser.add_argument('manpage_name')
    sub_parser.add_argument('section')
    sub_parser.add_argument('category')
    sub_parser.add_argument('command_name')
    sub_parser.add_argument('description')
    sub_parser.add_argument('--output-dir', help = 'output directory')
    sub_parser.add_argument('src', nargs = '+')

    sub_parser = add_parser(help2man, 'Producing man page from command help.')
    sub_parser.add_argument('manpage_name')
    sub_parser.add_argument('section')
    sub_parser.add_argument('version')
    sub_parser.add_argument('--output-dir', help = 'output directory')
    sub_parser.add_argument('cmd')
    sub_parser.add_argument('arg', nargs = argparse.REMAINDER)

    if sys.stdout.encoding.lower() != util.utf8:
        raise parser.error('No UTF-8-enabled locale set in environment.'
            'Please check your locale settings (particulary LANG, LC_ALL).')

    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    global_runtime_setup()
    args.subcommand(args)
    return 0


"""Production of man pages using pandoc or GNU help2man."""

# Author: 2015 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['write_man', 'of_help', 'of_markdown']


from collections.abc import Sequence
import re
from os import path
import os
import subprocess
import locale
import shlex
import tempfile

from . import util
from .pandoc import pandoc


def the_locale_string() -> str:

    code, encoding = locale.getdefaultlocale()
    return (code if code is not None else 'C') + \
      ('.' + encoding if encoding is not None else '')


def write_man(loc: str, name: str, section: str, content: str) -> None:

    if not path.isdir(loc):
        os.makedirs(loc)
    dst = path.join(loc, '{0}.{1}'.format(name, section))
    with open(dst, 'w') as writer:
        writer.write(content)


def of_help(name: str, version: str, section: str, args: Sequence[str]) -> str:

    with tempfile.TemporaryDirectory() as loc:
        executable = path.join(loc, name)
        wrapper = [
          '#!/bin/sh',
          '',
          'exec ' + ' '.join(shlex.quote(arg) for arg in args) + ' "$@"',
          ''
        ]
        with open(executable, 'w') as writer:
            writer.write('\n'.join(wrapper))
        os.chmod(executable, 0o755)
        try:
            content = util.pipe_through(None, ['help2man', '--no-info',
              '--locale={0}'.format(the_locale_string()),
              '--version-string={0}'.format(version),
              '--section={0}'.format(section), executable])
        except subprocess.CalledProcessError:
            heading = 'Output for call {0} --help:'.format(
              ' '.join(shlex.quote(arg) for arg in args))
            subprocess.run(['bash', '-c', 'echo {} >&2; {} --help'.
              format(shlex.quote(heading), shlex.quote(executable))])
            raise
        os.remove(executable)
    return content


re_section_heading = re.compile(r'^(\.SH .*)$', re.MULTILINE)
re_link = re.compile(r' \(#[^)]+\)$', re.MULTILINE)

def of_markdown(manpage_name: str, date: str, section: str, category: str,
  command_name: str, description: str, srcs: Sequence[str]) -> str:

    txt1 = pandoc('% {0}\n\n# Name\n**{1}** - {2}\n'.format(manpage_name,
      command_name, description), srcs, 'man',
      date = date, section = section, title = manpage_name,
      description = '"' + category + '"')
    txt2 = re_section_heading.sub(lambda match: match.group(1).upper(), txt1)
    txt3 = re_link.sub('', txt2)
    return txt3


"""Antiquotations operating on pandoc markdown ASTs."""

# Author: 2012 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['block', 'inline']


from types import EllipsisType
from typing import TypeAlias, Any
from collections.abc import Sequence, Mapping, Callable
import itertools
import os
from os import path
import subprocess
import shlex
import re

from debian.deb822 import Deb822

from . import dynamic
from . import pandoc
from . import param_spec
from . import sphinx_argparse


pattern_antiquote = R'\{(?P<name>\w+?)(?P<args>(?:\s+\S+?)*?)\s*\}'
re_arg = re.compile(R'\s*(\S+)')


class Antiquote_Exception(Exception):

    def __init__(self, markup_prefix: str, name: str, arguments: Sequence[str]) -> None:

        self.name = name
        self.arguments = arguments
        message = 'While processing antiquotation ' + markup_prefix \
          + '{' + ' '.join(itertools.chain([name], arguments)) + '}'
        super(Antiquote_Exception, self).__init__(message)


embedding: TypeAlias = Callable[[str], Any]
joining: TypeAlias = Callable[[Sequence[Any]], Any]
antiquotating: TypeAlias = Callable[..., Any]


def parse(markup_prefix: str, embed: embedding, join: joining, txt: str) \
  -> tuple[Sequence[tuple[str, Sequence[str]]], joining]:

    assert isinstance(txt, str)

    re_antiquote = re.compile(re.escape(markup_prefix) + pattern_antiquote)

    txts = []
    antiquote_exprs: list[tuple[str, list[str]]] = []
    lastindex = 0
    for match in re_antiquote.finditer(txt):
        txts.append(txt[lastindex:match.start(0)])
        name = match.group('name')
        args = list(match.group(1) for match in re_arg.finditer(match.group('args')))
        antiquote_exprs.append((name, args))
        lastindex = match.end(0)
    txts.append(txt[lastindex:])

    def insert_antiquotes(antiquotes: Sequence[Any]) -> Any:

        assert len(antiquotes) == len(antiquote_exprs)

        return join(list(x for txt, value in zip(txts, [*antiquotes, embed('')])
          for x in [embed(txt), value]))

    return antiquote_exprs, insert_antiquotes


def join_text(txts: Sequence[str]) -> str:

    return ''.join(txts)


def extended(markup_prefix: str, antiquotations: Mapping[str, antiquotating],
  embed: embedding, join: joining, txt: str) -> Any:

    antiquote_exprs, insert_antiquotes = parse(markup_prefix, embed, join, txt)

    antiquotes = []
    for name, args in antiquote_exprs:
        if name not in antiquotations:
            raise KeyError(f'No such antiquotation: »{name}«')
        antiquotation = antiquotations[name]
        try:
            antiquote = antiquotation(*args)
        except Exception as e:
            raise Antiquote_Exception(markup_prefix, name, args) from e
        antiquotes.append(antiquote)

    return insert_antiquotes(antiquotes)


def explode_ident(txt: str) -> Sequence[str]:

    return txt.split('.')


def check_executable(prefix: str, suffix: str) -> str:

    loc = path.join(prefix, suffix)
    if not path.isfile(loc) or os.stat(loc).st_mode & 0o755 != 0o755:
        raise Exception('Not an executable: {}'.format(loc))

    return suffix


def get_bash_variable(loc: str, var: str) -> str:

    safe_var = shlex.quote(var)
    if safe_var != var:
        raise Exception('Bad shell variable: {}'.format(var))

    proc = subprocess.run(['bash', '-c', 'source ' + shlex.quote(loc) +
      '; echo -n "${' + safe_var + '}"'], stdout = subprocess.PIPE, text = True, check = True)

    return proc.stdout


identval_name = 'identval'
exprval_name = 'exprval'
identname_name = 'identname'
file_name = 'file'
executable_name = 'executable'
bash_variable_name = 'bash_variable'
control_name = 'control'
function_name = 'function'
argument_name = 'argument'
argspec_name = 'argspec'
include_name = 'include'


def argspec(*args: str) -> Any:

    assert len(args) == 3 and args[1] == 'for'
    ident_parser, _, loc = args

    loc_base = check_executable('', loc)

    parser = dynamic.retrieve(explode_ident(ident_parser))
    parser.prog = loc_base
    parser_data = sphinx_argparse.parse_parser(parser,  # type: ignore
      data = dict(name = loc_base, usage = parser.format_usage()))

    def singleton_plaintext_paragraph(txt: str) -> list[Any]:
        return [pandoc.paragraph(pandoc.plaintext(txt))]

    entries = [(pandoc.plaintext(parser_data['description']),
      singleton_plaintext_paragraph(str(parser_data['usage'].rstrip())))]
    pos_args = parser_data.get('args', None)
    if pos_args:
        entries.append((pandoc.plaintext('positional arguments'),
          [pandoc.definition_list(
            [([pandoc.emphasized(arg['name'])],
              singleton_plaintext_paragraph(arg['help']))
              for arg in pos_args])]))
    options = parser_data.get('options', None)
    if options:
        entries.append((pandoc.plaintext('optional arguments'),
          [pandoc.definition_list(
            [([pandoc.emphasized(', '.join(ident for ident in arg['name']))],
              singleton_plaintext_paragraph(arg['help']))
              for arg in options])]))

    return pandoc.definition_list(entries)


def include(*args: str) -> Any:

    assert len(args) >= 1
    src = args[0]
    raw_attrs = args[1:]

    with open(src) as reader:
        content = reader.read()

    tag = ''
    classes = []
    attrs = []
    for raw_attr in raw_attrs:
        if raw_attr.startswith('#'):
            tag = raw_attr[1:]
        elif raw_attr.startswith('.'):
            classes.append(raw_attr[1:])
        elif '=' in raw_attr:
            attrs.append(raw_attr.split('=', 2))
        else:
            raise ValueError('Bad attribute for pandoc code block: {}'.format(raw_attr))

    return pandoc.code_block(content, tag, classes, attrs)


def file(*args: str) -> Any:

    assert len(args) == 1 or (len(args) == 3 and args[1] == 'in')
    suffix = args[0]
    prefix = args[2] if len(args) == 3 else ''
    loc = path.join(prefix, suffix)

    if not path.exists(loc):
        raise Exception('No such file: {}'.format(loc))

    return pandoc.plaintext(suffix)


def executable(*args: str) -> Any:

    assert len(args) == 1 or (len(args) == 3 and args[1] == 'in')
    suffix = args[0]
    prefix = args[2] if len(args) == 3 else ''

    loc = check_executable(prefix, suffix)

    return pandoc.plaintext(loc)


def bash_variable(*args: str) -> Any:

    assert len(args) == 3 and args[1] == 'in'
    name, _, loc = args

    return pandoc.plaintext(get_bash_variable(loc, name))


def control(*args: str) -> Any:

    assert len(args) == 1 or (len(args) == 3 and args[1] == 'in')

    field = args[0]
    src = args[2] if len(args) == 3 else 'debian/control'

    with open(src) as reader:
        control = Deb822(reader, fields = [field])
    value = control[field]
    return pandoc.plaintext(value)


def get_identspec(*args: str) -> Any:

    assert len(args) == 1 or (len(args) == 3 and args[1] == 'in')
    if len(args) == 3:
        ident, _, identprefix = args
        return ident, dynamic.retrieve([*explode_ident(identprefix), *explode_ident(ident)])
    else:
        ident = args[0]
        return ident, dynamic.retrieve(explode_ident(ident))


def identname(*args: str) -> Any:

    ident, _ = get_identspec(*args)
    return pandoc.plaintext(ident)


def identval(*args: str) -> Any:

    return pandoc.plaintext(' '.join(str(dynamic.retrieve(explode_ident(ident))) for ident in args))


def exprval(*args: str) -> Any:

    return pandoc.plaintext(str(eval(' '.join(args))))


def function(*args: str) -> Any:

    ident, f = get_identspec(*args)
    return pandoc.plaintext(ident + param_spec.text_of(param_spec.retrieve(f)))


def argument(*args: str) -> Any:

    assert len(args) == 3 and args[1] == 'of'
    arg, _, ident = args

    f = dynamic.retrieve(explode_ident(ident))
    sign = param_spec.retrieve(f)

    if arg not in sign.parameters:
        raise Exception('No such argument: {} in function {}'.format(arg, ident))

    return pandoc.plaintext(arg)


block_antiquotations: dict[str, antiquotating] = {
  argspec_name: argspec,
  include_name: include
}

inline_antiquotations: dict[str, antiquotating] = {
  file_name: file,
  executable_name: executable,
  bash_variable_name: bash_variable,
  control_name: control,
  identname_name: identname,
  identval_name: identval,
  exprval_name: exprval,
  function_name: function,
  argument_name: argument
}


def join_block(xs: Sequence[str | None | EllipsisType]) -> str | None | EllipsisType:

    ys = [x for x in xs if x is not None]
    if all(y is Ellipsis for y in ys):
        return None
    elif len(ys) == 1:
        y = ys[0]
        return y
    else:
        raise Exception('Block antiquotation used inline rather than stand-alone.')


def block(txt: str) -> Any:

    return extended(markup_prefix = '@#',
      antiquotations = block_antiquotations,
      embed = lambda txt: Ellipsis if txt.strip() else None, join = join_block,
      txt = txt)


def inline(txt: str) -> Any:

    return extended(markup_prefix = '@',
      antiquotations = inline_antiquotations,
      embed = pandoc.plaintext, join = lambda xss: [x for xs in xss for x in xs],
      txt = txt)


"""Wrapper around pandoc and auxiliary for pandoc filters."""

# Author: 2015 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['pandoc', 'plaintext', 'emphasized', 'verbatim',
  'paragraph', 'definition_list', 'code_block',
  'map_plaintext', 'modify_plaintext']


from typing import Any, Optional, TextIO
from collections.abc import Mapping, Sequence, Callable
from pathlib import Path
import json

from . import util
from . import resources


def the_filter() -> Path:

    return resources.location_of('docmaker', 'helper', 'pandoc-json-filter')


def pandoc(prelude: str, srcs: Sequence[str], format: str, **variables: str) -> str:

    contents = []
    for src in srcs:
        with open(src) as reader:
            for line in reader:
                contents.append(line)
    txt = prelude + ''.join(contents)

    args = ['pandoc', '--standalone', '--toc'] + \
      ['--variable={0}:{1}'.format(key, variables[key]) for key in variables] + \
      ['--from=markdown', f'--filter={the_filter()}', '--to=' + format]

    return util.pipe_through(txt, args)


def plaintext(txt: str) -> Sequence[Any]:

    ys = []
    first = True
    for fragment in txt.split(' '):
        if not first:
            ys.append(dict(t = 'Space', c = []))
        first = False
        ys.append(dict(t = 'Str', c = fragment))
    return ys


def emphasized(txt: str) -> Any:

    return dict(t = 'Emph', c = plaintext(txt))


def verbatim(txt: str) -> Any:

    return dict(t = 'Code', c = (('', (), ()), txt))


def paragraph(xs: Sequence[Any]) -> Any:

    return dict(t = 'Para', c = xs)


def definition_list(entries: Sequence[tuple[Sequence[Any], Any]]) -> Any:

    return dict(t = 'DefinitionList', c = [(key, [blocks]) for key, blocks in entries])


def code_block(txt: str, tag: str = '', classes: Sequence[str] = [], attrs: Sequence[Sequence[str]] = []) -> Any:

    return dict(t = 'CodeBlock', c = ((tag, classes, attrs), txt))


def peek_plaintext(x: Any) -> Optional[str]:

    if not isinstance(x, dict):
        return None

    if x.get('t') == 'Str':
        return x.get('c')
    elif x.get('t') in ('Space', 'SoftBreak'):
        return ' '
    else:
        return None


def peek_all_plaintext(xs: Any) -> Optional[str]:

    if not isinstance(xs, list):
        return None

    txts = [peek_plaintext(x) for x in xs]
    return ''.join(txts) if all(txt is not None for txt in txts) else None  # type: ignore


def map_plaintext(f_block: Callable[[str], Any], f_inline: Callable[[str], Any], d: Any) -> Any:

    def map(x: Any) -> Any:

        if isinstance(x, dict):
            return map_node(x)
        elif isinstance(x, list):
            return map_list(x)
        else:
            return x

    def map_node(d: Mapping[str, Any]) -> Mapping[str, Any]:

        t = dict()
        for key in d:
            t[key] = map(d[key])
        return t

    def perhaps_map_toplevel_paragraph(x: Any) -> Optional[Any]:

        if x['t'] != 'Para':
            return None
        some_txt = peek_all_plaintext(x['c'])
        if some_txt is None:
            return None
        return f_block(some_txt)

    def map_list(xs: Sequence[Any], toplevel: bool = False) -> Sequence[Any]:

        ys = []
        pending_plaintext: list[str] = []
        for x in xs:
            txt = peek_plaintext(x)
            if txt is None:
                if pending_plaintext:
                    ys += f_inline(''.join(pending_plaintext))
                    pending_plaintext = []
                some_block = perhaps_map_toplevel_paragraph(x) \
                  if toplevel else None
                if some_block is not None:
                    ys.append(some_block)
                else:
                    ys.append(map(x))
            else:
                pending_plaintext.append(txt)
        if pending_plaintext:
            ys += f_inline(''.join(pending_plaintext))
        return ys

    d['blocks'] = map_list(d['blocks'], toplevel = True)
    return d


def modify_plaintext(f_block: Callable[[str], Any], f_inline: Callable[[str], Any],
  reader: TextIO, writer: TextIO) -> None:

    x = json.load(reader)
    y = map_plaintext(f_block, f_inline, x)
    json.dump(y, writer)

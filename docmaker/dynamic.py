
"""Dynamic handling of modules."""

# Author: 2010 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This package is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>
#
# On Debian systems, the complete text of the GNU General
# Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


__all__ = ['retrieve']


from typing import Any
from collections.abc import Sequence
import builtins
import sys


def name_of_access(access: Sequence[str]) -> str:

    for fragment in access:
        assert '.' not in fragment
        assert fragment != ''

    return '.'.join(access)


def retrieve(access: Sequence[str]) -> Any:

    focus = builtins
    for i in range(len(access)):
        subaccess = access[0:i+1]
        name = name_of_access(subaccess)
        if hasattr(focus, access[i]):
            focus = getattr(focus, access[i])
        elif name in sys.modules:
            focus = sys.modules[name]
        else:
            focus = __import__(name, fromlist = [''])
            # explicit formal fromlist ensures that __import__ returns module, not package

    return focus

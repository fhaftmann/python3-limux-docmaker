
"""Generalized view on arbitrary callable python objects."""

# Author: 2016 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['retrieve', 'text_of']


from typing import Any
from collections.abc import Callable
import inspect


def retrieve(f: Callable[..., Any]) -> inspect.Signature:

    return inspect.signature(f)


def text_of(argspec: inspect.Signature) -> str:

    return str(argspec)

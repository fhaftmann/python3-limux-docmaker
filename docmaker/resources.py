
"""Resource files accompanying python packages."""

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from collections.abc import Sequence
from pathlib import Path
import sys


__all__ = ['location_of']


def implode(ps: Sequence[str]) -> Path:

    return Path(ps[0]).joinpath(*ps[1:])


def location_of(package: str, *res: str) -> Path:

    some_pkg = sys.modules.get(package)
    if some_pkg is None or some_pkg.__file__ is None:
        raise ModuleNotFoundError(package)

    p = implode(res)
    p = p.relative_to(p.root)
    p = Path(some_pkg.__file__).parent / p

    if not p.exists():
        raise FileNotFoundError(p)

    return p

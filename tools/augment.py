#!/usr/bin/python3 -I

# Author: 2023 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.
#
# You may obtain a copy of the Licence at:
#
#   https://spdx.org/licenses/EUPL-1.1.html
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the Licence for the specific language governing
# permissions and limitations under the Licence.


from collections.abc import Sequence
from dataclasses import dataclass
from pathlib import Path
import os
import subprocess
import argparse
import argcomplete

import toml


prefix = Path('debian')
doc_src = Path('resources/documentation.md')


@dataclass(frozen = True)
class Project:
    pypackage: str
    version: str
    pycmds: Sequence[Path]
    doc_prefix: Path


def read_pyproject() -> Project:

    raw = toml.load('pyproject.toml')
    name = raw['project']['name']
    version = raw['project']['version']
    pypackage = f'python3-{name}'
    bin_prefix = prefix / pypackage / 'usr' / 'bin'
    doc_prefix = prefix / pypackage / 'usr' / 'share' / 'doc' / pypackage
    return Project(pypackage = pypackage, version = version,
      pycmds = [bin_prefix / s for s in raw['project']['scripts'].keys()],
      doc_prefix = doc_prefix)


def generate_man_pages(project: Project) -> None:

    for pycmd in project.pycmds:
        subprocess.run(['python3', '-c' 'from docmaker import cmd; cmd.cmd()',
          'help2man', '--output-dir', 'man',
          pycmd.name, '1', project.version, 'python3', pycmd],
          env = os.environ | dict(PYTHONPATH = '.'),
          check = True)


def generate_bash_completions(project: Project) -> None:

    completion_summary = prefix / f'{project.pypackage}.bash-completion'
    completion_prefix = prefix / f'{project.pypackage}.bash-completions'

    completions = []
    completion_prefix.mkdir(exist_ok = True)
    for pycmd in project.pycmds:
        name = pycmd.name
        result = subprocess.run(['register-python-argcomplete', name],
          stdout = subprocess.PIPE, text = True, check = True)
        completion = completion_prefix / name
        completion.write_text(result.stdout)
        completions.append(f'{completion}\n')

    completion_summary.write_text(''.join(completions))


def generate_documentation(project: Project) -> None:

    project.doc_prefix.mkdir(parents = True, exist_ok = True)
    doc_dst = project.doc_prefix / f'{project.pypackage}.html'

    proc = subprocess.run(['python3', '-c' 'from docmaker import cmd; cmd.cmd()',
      'html', f'{project.pypackage} {project.version}', 'resources/documentation.md'],
      env = os.environ | dict(PYTHONPATH = '.'),
      stdout = subprocess.PIPE, text = True,
      check = True)

    doc_dst.write_text(proc.stdout)


parser = argparse.ArgumentParser()
argcomplete.autocomplete(parser)
args = parser.parse_args()

os.chdir(Path(__file__).parent)

project = read_pyproject()

generate_bash_completions(project)
generate_man_pages(project)
generate_documentation(project)

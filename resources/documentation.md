
# Ziele

*docmaker* ist ein kleines Metatool, um technische Dokumentation von Softwarepaketen
einheitlich und übersichtlich zu erstellen und zu pflegen.

Die Idee von *docmaker* ist, dass Dokumentation als Klartextformat *zusammen*
mit den zugehörigen Softwarequellen verwaltet, gepflegt und compiliert wird.
Damit ist eine einheitliche Zugänglichkeit und Historie von Software und
Dokumentation begünstigt.

Ergänzt wird das Klartextformat durch sogenannte *Antiquotations*, die auf formale
Entitäten (Python-Artefakte, Dateien, …)
referenzieren und somit zur Sicherung der Konsistenz eingesetzt werden können.

Integriert ist eine Rechtschreibprüfung mit persistenten Wörterbüchern.  Dadurch
wird die Verwendung einer einheitlichen Sprache gefördert.


# Dokumentation erstellen

*docmaker* verwendet [*pandoc*](http://johnmacfarlane.net/pandoc), um aus
Klartextdateien HTML-Seiten und Manpages zu erstellen.  Die Klartextdateien
werden dabei in *markdown* geschrieben.

Eine ausführliche Beschreibung des Klartextformats findet sich unter
<http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown>.  Als
Textkodierung wird einheitlich *utf-8* verwendet (siehe dazu auch
<http://utf8everywhere.org/>).

Die Klartextdateien lassen sich dann wie folgt nach HTML umsetzen:

> `docmaker html`\ *Titel Eingabedatei* [*Eingabedatei* …]

Für die Erstellung einer Manpage sind mehr Metadaten erforderlich:

> `docmaker man`\ *Name Sektion Kategorie Command-Name Beschreibung* [*Eingabedatei* …]


# Antiquotations

Antiquotations haben die allgemeine Form `@{`*name* *arg~1~* … *arg~n~*`}`.  Folgende
konkrete Antiquotations stehen zur Verfügung:

@{identval docmaker.antiquote.file_name}\ *location* [`in` *prefix*]

:   Es wird zugesichert, dass die Datei *prefix*/*location* existiert;  *location* wird eingesetzt.

@{identval docmaker.antiquote.executable_name}\ *location* [`in` *prefix*]

:   Es wird zugesichert, dass *prefix*/*location* eine ausführbare Datei ist;  *location* wird eingesetzt.

@{identval docmaker.antiquote.bash_variable_name}\ *ident* `in` *sourcelet*

:   Das *sourcelet* wird von einer *Bash* gesourct und der anschließende Wert der Shell-Variablen *ident*
    eingesetzt.

@{identval docmaker.antiquote.control_name}\ *ident* [`in` *control*]

:   Die Debian-RFC822-konforme Datei *control* wird eingelesen
    (per Default *debian/control*) und der Wert des Eintrags
    *ident* darin eingesetzt.

@{identval docmaker.antiquote.identname_name}\ *ident* [`in` *identprefix*]

:   Der Python-Identifier *identprefix*.*ident* wird im aktuell gültigen Modulraum implizit importiert;
    ein erfolgreicher Import wird zugesichert.  Es wird *ident* eingesetzt.

@{identval docmaker.antiquote.identval_name}\ *ident* …

:   Jeder Python-Identifier *ident* wird im aktuell gültigen Modulraum implizit importiert
    und dereferenziert.  Für jedes Ergebnis wird jeweils seine Textdarstellung eingesetzt.

@{identval docmaker.antiquote.exprval_name}\ *arg* …

:   Alle Argumente *arg* werden als Python-Ausdruck mit Leerraum zwischen jeweils zwei
    Argumenten ausgewertet.  Die Textdarstellung des Ergebnisses wird eingesetzt.

@{identval docmaker.antiquote.function_name}\ *ident* `in` *identprefix*

:   Der Python-Identifier *identprefix*.*ident* wird im aktuell gültigen Modulraum implizit importiert;
    es wird zugesichert, dass er für eine Funktion steht.  Deren Signatur wird eingesetzt.

@{identval docmaker.antiquote.argument_name}\ *arg* `of` *ident*

:   Der Python-Identifier *ident* wird im aktuell gültigen Modulraum implizit importiert;
    es wird zugesichert, dass er für eine Funktion steht und *arg* ein Parameter davon ist.
    Dieses *arg* wird eingesetzt.


# Block-Antiquotations

Block-Antiquotations unterscheiden sich wiefolgt von gewöhnlichen (Inline-)Antiquotations:

* Das einleitende Symbol ist `@#` statt `@`.

* Eine Block-Antiquotation setzt einen ganzen Textblock ein und darf
daher nur als alleiniges Element in einem Absatz vorkommen.

Folgende Block-Antiquotations stehen zur Verfügung:

@{identval docmaker.antiquote.argspec_name}\ *parser* `for` *location*

:   Es wird zugesichert, dass *location* eine ausführbare Datei ist;  die Argumentspezifikation
    der *argparse*-Instanz *parser* wird eingesetzt.

@{identval docmaker.antiquote.include_name}\ *location* *attribute* …

:   Der Inhalt der Datei *location* wird als Codeblock eingesetzt, wobei alle *attribute*
    als Attribute in den Kopf des Codeblocks eingesetzt werden.


# Dokumentation überprüfen

*docmaker* verwendet [*aspell*](http://aspell.net) zur Rechtschreibprüfung.
Eine interaktive Rechtschreibprüfung kann gestartet werden mit

> `docmaker spellimprove`\ *Sprache Wörterbuch* [*Eingabedatei* …]

Dabei ist *Sprache* ein von *aspell* akzeptierter Sprachcode (z.B. `de_DE`)
und *Wörterbuch* ein Verzeichnis, worin die Wörterbuchdateien abgelegt werden,
deren Wörter über das Standardwörterbuch hinaus gehen.

Eine nicht-interaktive Rechtschreibprüfung erfolgt mit

> `docmaker spellcheck`\ *Sprache Wörterbuch* [*Eingabedatei* …]

Damit kann formal geprüft werden, ob die Dokumentationsquellen im Sinne
der Rechtschreibprüfung in Ordnung sind.

Nicht geprüft wird der Inhalt von Antiquotations.


# Verwendung in Debian-Paketen

*docmaker* ist darauf zurechtgeschnitten, im Buildprozess von Debianpaketen
zum Einsatz zu kommen.  Als Template hierzu kann das Sourcepaket von
*@{control Source}* selbst dienen.  Die Build-Abhängigkeiten des Pakets
müssen dabei um *@{control Source}* und ggf.\ Wörterbücher für *aspell*
(z.B. *aspell-de*) ergänzt werden.
